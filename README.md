## How to run:
* git clone https://gitlab.com/DShvarts/password-education-webapp/
* unzip password-education-webapp
* cd password-security-app
* nvm use v15
* npm install
* npm run

Site will be hosted on the default localhost:3000 address.
